gradle-shadow-plugin-bug
========================

Description
------------
Sample project demonstrates problem with shadow gradle plugin. 

If task is moved from root "gradle.build" to separate file i get cast exception -> "Cannot cast object 'org.gradle.api.plugins.shadow.transformers.AppendingTransformer@59628381' with class 'org.gradle.api.plugins.shadow.transformers.AppendingTransformer' to class 'org.gradle.api.plugins.shadow.transformers.Transformer'"

Run 
------------
$> gradle

Full stack trace 
----------------

     FAILURE: Build failed with an exception.
   
     * Where:
     Script 'D:\Users\loans_idea\loans\support\gradle\shadow.gradle' line: 84
    
     * What went wrong:
     A problem occurred evaluating script.
     > Cannot cast object 'org.gradle.api.plugins.shadow.transformers.AppendingTransformer@96a32aa' with class 'org.gradle.api.plugins.shadow.transformers.AppendingTransformer' to class 'org.gradle.api.plugins.shadow.transformers.Transformer'
     
     * Exception is:
     org.gradle.api.GradleScriptException: A problem occurred evaluating script.
            at org.gradle.groovy.scripts.internal.DefaultScriptRunnerFactory$ScriptRunnerImpl.run(DefaultScriptRunnerFactory.java:54)
            at org.gradle.configuration.DefaultScriptPluginFactory$ScriptPluginImpl.apply(DefaultScriptPluginFactory.java:131)
            at org.gradle.api.internal.plugins.DefaultObjectConfigurationAction.applyScript(DefaultObjectConfigurationAction.java:82)
            at org.gradle.api.internal.plugins.DefaultObjectConfigurationAction.access$000(DefaultObjectConfigurationAction.java:32)
            at org.gradle.api.internal.plugins.DefaultObjectConfigurationAction$1.run(DefaultObjectConfigurationAction.java:54)
            at org.gradle.api.internal.plugins.DefaultObjectConfigurationAction.execute(DefaultObjectConfigurationAction.java:114)
            at org.gradle.api.internal.project.AbstractPluginAware.apply(AbstractPluginAware.java:39)
            at org.gradle.api.internal.BeanDynamicObject$MetaClassAdapter.invokeMethod(BeanDynamicObject.java:248)
            at org.gradle.api.internal.BeanDynamicObject.invokeMethod(BeanDynamicObject.java:136)
            at org.gradle.api.internal.CompositeDynamicObject.invokeMethod(CompositeDynamicObject.java:147)
            at org.gradle.api.internal.project.DefaultProject_Decorated.invokeMethod(Unknown Source)
            at org.gradle.api.internal.project.ProjectScript.apply(ProjectScript.groovy:34)
            at org.gradle.api.Script$apply.callCurrent(Unknown Source)
            at build_4e10nneaomu42e1afiuvfe3k4c.run(D:\Users\loans_idea\loans\modules-lt\build.gradle:6)
            at org.gradle.groovy.scripts.internal.DefaultScriptRunnerFactory$ScriptRunnerImpl.run(DefaultScriptRunnerFactory.java:52)
            at org.gradle.configuration.DefaultScriptPluginFactory$ScriptPluginImpl.apply(DefaultScriptPluginFactory.java:131)
            at org.gradle.configuration.project.BuildScriptProcessor.execute(BuildScriptProcessor.java:38)
            at org.gradle.configuration.project.BuildScriptProcessor.execute(BuildScriptProcessor.java:25)
            at org.gradle.configuration.project.ConfigureActionsProjectEvaluator.evaluate(ConfigureActionsProjectEvaluator.java:34)
            at org.gradle.configuration.project.LifecycleProjectEvaluator.evaluate(LifecycleProjectEvaluator.java:55)
            at org.gradle.api.internal.project.AbstractProject.evaluate(AbstractProject.java:469)
            at org.gradle.api.internal.project.AbstractProject.evaluate(AbstractProject.java:77)
            at org.gradle.configuration.DefaultBuildConfigurer.configure(DefaultBuildConfigurer.java:31)
            at org.gradle.initialization.DefaultGradleLauncher.doBuildStages(DefaultGradleLauncher.java:142)
            at org.gradle.initialization.DefaultGradleLauncher.doBuild(DefaultGradleLauncher.java:113)
            at org.gradle.initialization.DefaultGradleLauncher.run(DefaultGradleLauncher.java:81)
            at org.gradle.launcher.exec.InProcessBuildActionExecuter$DefaultBuildController.run(InProcessBuildActionExecuter.java:64)
            at org.gradle.launcher.cli.ExecuteBuildAction.run(ExecuteBuildAction.java:33)
            at org.gradle.launcher.cli.ExecuteBuildAction.run(ExecuteBuildAction.java:24)
            at org.gradle.launcher.exec.InProcessBuildActionExecuter.execute(InProcessBuildActionExecuter.java:35)
            at org.gradle.launcher.daemon.server.exec.ExecuteBuild.doBuild(ExecuteBuild.java:45)
            at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:34)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.WatchForDisconnection.execute(WatchForDisconnection.java:42)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.ResetDeprecationLogger.execute(ResetDeprecationLogger.java:24)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.StartStopIfBuildAndStop.execute(StartStopIfBuildAndStop.java:33)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.ReturnResult.execute(ReturnResult.java:34)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.ForwardClientInput$2.call(ForwardClientInput.java:70)
            at org.gradle.launcher.daemon.server.exec.ForwardClientInput$2.call(ForwardClientInput.java:68)
            at org.gradle.util.Swapper.swap(Swapper.java:38)
            at org.gradle.launcher.daemon.server.exec.ForwardClientInput.execute(ForwardClientInput.java:68)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.LogToClient.doBuild(LogToClient.java:60)
            at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:34)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.EstablishBuildEnvironment.doBuild(EstablishBuildEnvironment.java:59)
            at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:34)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.StartBuildOrRespondWithBusy$1.run(StartBuildOrRespondWithBusy.java:45)
            at org.gradle.launcher.daemon.server.DaemonStateCoordinator.runCommand(DaemonStateCoordinator.java:186)
            at org.gradle.launcher.daemon.server.exec.StartBuildOrRespondWithBusy.doBuild(StartBuildOrRespondWithBusy.java:49)
            at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:34)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.HandleStop.execute(HandleStop.java:36)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.CatchAndForwardDaemonFailure.execute(CatchAndForwardDaemonFailure.java:32)
            at org.gradle.launcher.daemon.server.exec.DaemonCommandExecution.proceed(DaemonCommandExecution.java:125)
            at org.gradle.launcher.daemon.server.exec.DefaultDaemonCommandExecuter.executeCommand(DefaultDaemonCommandExecuter.java:48)
            at org.gradle.launcher.daemon.server.DefaultIncomingConnectionHandler$ConnectionWorker.handleCommand(DefaultIncomingConnectionHandler.java:155)
            at org.gradle.launcher.daemon.server.DefaultIncomingConnectionHandler$ConnectionWorker.receiveAndHandleCommand(DefaultIncomingConnectionHandler.java:128)
            at org.gradle.launcher.daemon.server.DefaultIncomingConnectionHandler$ConnectionWorker.run(DefaultIncomingConnectionHandler.java:116)
            at org.gradle.internal.concurrent.DefaultExecutorFactory$StoppableExecutorImpl$1.run(DefaultExecutorFactory.java:66)
     Caused by: org.codehaus.groovy.runtime.typehandling.GroovyCastException: Cannot cast object 'org.gradle.api.plugins.shadow.transformers.AppendingTransformer@96a32aa' with class 'org.gradle.api.plugins.shadow.transformers.AppendingTransformer' to class 'org.gradle.api.plugins.shadow.transformers.Transformer'
            at org.gradle.api.plugins.shadow.ShadowTaskExtension.transformer(ShadowTaskExtension.groovy:80)
            at shadow_3bakimiqe679cf3um2vner0tnh$_run_closure4_closure9.doCall(D:\Users\loans_idea\loans\support\gradle\shadow.gradle:84)
            at org.gradle.api.internal.ClosureBackedAction.execute(ClosureBackedAction.java:58)
            at org.gradle.api.internal.plugins.ExtensionsStorage$ExtensionHolder.configure(ExtensionsStorage.java:149)
            at org.gradle.api.internal.plugins.ExtensionsStorage.configureExtension(ExtensionsStorage.java:73)
            at org.gradle.api.internal.plugins.DefaultConvention$ExtensionsDynamicObject.invokeMethod(DefaultConvention.java:216)
            at org.gradle.api.internal.CompositeDynamicObject.invokeMethod(CompositeDynamicObject.java:147)
            at org.gradle.api.internal.project.DefaultProject_Decorated.invokeMethod(Unknown Source)
            at shadow_3bakimiqe679cf3um2vner0tnh$_run_closure4.doCall(D:\Users\loans_idea\loans\support\gradle\shadow.gradle:83)
            at org.gradle.api.internal.ClosureBackedAction.execute(ClosureBackedAction.java:58)
            at org.gradle.util.ConfigureUtil.configure(ConfigureUtil.java:133)
            at org.gradle.util.ConfigureUtil.configure(ConfigureUtil.java:94)
            at org.gradle.api.internal.project.AbstractProject.configure(AbstractProject.java:857)
            at org.gradle.api.internal.project.AbstractProject.configure(AbstractProject.java:862)
            at org.gradle.api.internal.BeanDynamicObject$MetaClassAdapter.invokeMethod(BeanDynamicObject.java:248)
            at org.gradle.api.internal.BeanDynamicObject.invokeMethod(BeanDynamicObject.java:136)
            at org.gradle.api.internal.CompositeDynamicObject.invokeMethod(CompositeDynamicObject.java:147)
            at org.gradle.groovy.scripts.BasicScript.methodMissing(BasicScript.java:83)
            at shadow_3bakimiqe679cf3um2vner0tnh.run(D:\Users\loans_idea\loans\support\gradle\shadow.gradle:64)
            at org.gradle.groovy.scripts.internal.DefaultScriptRunnerFactory$ScriptRunnerImpl.run(DefaultScriptRunnerFactory.java:52)
            .. 65 more
